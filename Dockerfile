FROM docker.io/nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf
COPY /dist/angular-cicd-app /usr/share/nginx/html